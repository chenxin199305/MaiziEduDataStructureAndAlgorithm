package lesson_016;

import lesson_014.BinarySearchTreeNode;

public class BFSTreeSearch {

	public static int depth(BinarySearchTreeNode binarySearchTreeNode) {
		if (binarySearchTreeNode == null) {
			return 0;
		}
		
		int leftDepth 	= depth(binarySearchTreeNode.left);
		int rightDepth	= depth(binarySearchTreeNode.right);
		return Math.max(leftDepth, rightDepth) + 1;
	}
	
	/**
	 *	函数说明：	广度优先搜索算法，搜索第 target 层 
	 */
	public static void BFS_search(BinarySearchTreeNode binarySearchTreeNode, int targetLevel) {
		
		searchPrint(binarySearchTreeNode, targetLevel);
	}
	
	public static void searchPrint(BinarySearchTreeNode binarySearchTreeNode, int targetLevel) {
		
		if (binarySearchTreeNode == null) {
			return;
		}
		
		if (targetLevel == 0) {
			System.out.println(binarySearchTreeNode.value);
			return;
		}

		searchPrint(binarySearchTreeNode.left, targetLevel - 1);
		searchPrint(binarySearchTreeNode.right, targetLevel - 1);
	}
	
	public static void main(String[] args) {
		
		int[] treeNodeValues = new int[]{4,2,1,3,6,5,7};
		BinarySearchTreeNode root = new BinarySearchTreeNode(treeNodeValues[0]);
		for (int i = 1; i < treeNodeValues.length; i++) {
			root.add(treeNodeValues[i]);
		}
		
		System.out.println(depth(root));
		BFS_search(root, 2);
	}
}
