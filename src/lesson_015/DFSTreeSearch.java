package lesson_015;

import lesson_014.BinarySearchTreeNode;

public class DFSTreeSearch {

	public static void DFS_forehead(BinarySearchTreeNode binarySearchTreeNode) {
		if (binarySearchTreeNode == null) {
			return;
		}

		System.out.println(binarySearchTreeNode.value);
		DFS_forehead(binarySearchTreeNode.left);
		DFS_forehead(binarySearchTreeNode.right);
	}
	
	public static void DFS_meadian(BinarySearchTreeNode binarySearchTreeNode) {
		if (binarySearchTreeNode == null) {
			return;
		}

		DFS_meadian(binarySearchTreeNode.left);
		System.out.println(binarySearchTreeNode.value);
		DFS_meadian(binarySearchTreeNode.right);
	}

	public static void DFS_hindside(BinarySearchTreeNode binarySearchTreeNode) {
		if (binarySearchTreeNode == null) {
			return;
		}

		DFS_hindside(binarySearchTreeNode.left);
		DFS_hindside(binarySearchTreeNode.right);
		System.out.println(binarySearchTreeNode.value);
	}
	
	public static void main(String[] args) {
		int[] treeNodeValues = new int[]{4,2,1,3,6,5,7};
		BinarySearchTreeNode root = new BinarySearchTreeNode(treeNodeValues[0]);
		for (int i = 1; i < treeNodeValues.length; i++) {
			root.add(treeNodeValues[i]);
		}
		
		DFS_hindside(root);
	}
}
