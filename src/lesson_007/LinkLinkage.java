package lesson_007;

import lesson_002.Node;

public class LinkLinkage {

	/**
	 *	方法说明：	将两个已经排好序的链表进行合并
	 *
	 * 	@param		node1	链表1
	 * 	@param		node2 	链表2
	 * 
	 * 	笔记：		这是一种非递归的算法，实际上也可以用递归来做
	 */
	public static Node linkLinkage(Node node1, Node node2) {
		
		// 特殊情况
		if (node1 == null) {
			return node2;
		}
		if (node2 == null) {
			return node1;
		}
		
		// 初始化 result
		Node resultHead = null;
		Node resultTemp = null;
		if (node1.value < node2.value) {
			resultHead = node1;
			node1 = node1.next;
		}
		else {
			resultHead = node2;
			node2 = node2.next;
		}
		
		resultTemp = resultHead;
		// 遍历链表
		while (node1 != null || node2 != null) {
			if (node1 == null) {
				resultTemp.next = node2;
				break;
			}
			if (node2 == null) {
				resultTemp.next = node1;
				break;
			}
			
			if (node1.value < node2.value) {
				resultTemp.next = node1;
				node1 = node1.next;
			}
			else {
				resultTemp.next = node2;
				node2 = node2.next;
			}
			
			resultTemp = resultTemp.next;
		}
		
		return resultHead;
	}
	
	public static void main(String[] args) {
		Node node11 = new Node(1);
		Node node12 = new Node(2);
		Node node13 = new Node(3);
		Node node14 = new Node(4);
		Node node15 = new Node(5);

		node11.next = node12;
		node12.next = node13;
		node13.next = node14;
		node14.next = node15;
		node15.next = null;
		
		Node node21 = new Node(1);
		Node node22 = new Node(2);
		Node node23 = new Node(3);
		Node node24 = new Node(4);
		Node node25 = new Node(5);

		node21.next = node22;
		node22.next = node23;
		node23.next = node24;
		node24.next = node25;
		node25.next = null;
		
		Node result =linkLinkage(node11, node21);
		
		while (result != null) {
			System.out.print(result.value + " ");
			result = result.next;
		}
	}
}
