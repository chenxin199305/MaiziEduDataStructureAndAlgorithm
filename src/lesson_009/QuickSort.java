package lesson_009;

/**
 *	笔记：
 *			快速排序，先选定一个数作为轴心，
 *			放在中心，比它大的放左边，比它小的放右边 
 *
 *	想法：
 *			1. 方法一：递归实现，先分大小，后递归
 *			2. 方法二：在分大小的时候，可以用两个指针 leftPointer, 
 *						rightPointer 分别指向左右两端，
 *						然后从两端不断逼近中间，
 *						如果 left >= right，则结束，
 *						否则，要么将leftPointer左侧元素向leftPointer左侧移动，
 *						要么将 rightPointer 元素向 rightPointer 右侧移动
 *
 *	计算复杂度：
 *			1. T(n) ≈ 2 * T(n / 2)
 */
public class QuickSort {

	public static int[] quickSort(int[] array) {
		
		/**
		 *	笔记：
		 *			递归算法的第一步往往是写结束条件 
		 */
		if (array.length <= 1) {
			return array;
		}
		
		int[] result = new int[array.length];
		int core = array[0];
		
		// 计算左右数组大小
		int leftSize = 0;
		int rightSize = 0;
		for (int i = 1; i < array.length; i++) {
			if (array[i] < core) {
				leftSize ++;
			}
			else {
				rightSize ++;
			}
		}
		
		// 拷贝左右数组元素
		int leftArray[] = new int[leftSize];
		int rightArray[] = new int[rightSize];
		int leftCounter = 0;
		int rightCounter = 0;
		for (int i = 1; i < array.length; i++) {
			if (array[i] < core) {
				leftArray[leftCounter] = array[i];
				leftCounter++;
			}
			else {
				rightArray[rightCounter] = array[i];
				rightCounter++;
			}
		}
		
		// 递归
		leftArray = quickSort(leftArray);
		rightArray = quickSort(rightArray);
		
		// 拷贝全部数据
		for (int i = 0; i < leftArray.length; i++) {
			result[i] = leftArray[i];
		}
		result[leftArray.length] = core;
		for (int i = leftArray.length + 1; i < result.length; i++) {
			result[i] = rightArray[i - leftArray.length - 1];
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		
		int[] array = new int[]{3,4,4,5,3,2,3,4,6,5,5,4,3};
		
		int[] sortResult = quickSort(array);
	
		for (int i = 0; i < sortResult.length; i++) {
			System.out.print(sortResult[i] + " ");
		}
	}
}
