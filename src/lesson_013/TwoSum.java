package lesson_013;

public class TwoSum {

	/**
	 *	函数说明：	在已经排序好的数组上进行查找 
	 */
	public static void twoSum(int[] array, int target) {
		int left = 0;
		int right = array.length - 1;
		while (left <= right) {
			if (array[left] + array[right] > target) {
				right--;
			}
			else if (array[left] + array[right] < target) {
				left++;
			}
			else {
				System.out.println("left = " + left + " " + "right = " + right);
				left++; right--;
			}
		}
	}
	
	public static void main(String[] args) {
		
		int[] array = {0,1,2,3,4,5,6,7,8,9};
		twoSum(array, 9);
	}
}
