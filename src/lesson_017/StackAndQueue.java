package lesson_017;

import java.util.Stack;

import lesson_014.BinarySearchTreeNode;

public class StackAndQueue {

	public static void DFS_forehead(BinarySearchTreeNode binarySearchTreeNode) {

		Stack<BinarySearchTreeNode> stack = new Stack<>();
		while (binarySearchTreeNode != null) {

			System.out.println(binarySearchTreeNode.value);

			// 右
			if (binarySearchTreeNode.right != null) {
				stack.push(binarySearchTreeNode.right);
			}

			// 左
			if (binarySearchTreeNode.left != null) {
				binarySearchTreeNode = binarySearchTreeNode.left;
			}
			else {
				if (stack.isEmpty()) {
					binarySearchTreeNode = null;
				}
				else {
					binarySearchTreeNode = stack.pop();
				}
			}

			// 中
		}
	}

	public static void DFS_median(BinarySearchTreeNode binarySearchTreeNode) {

		Stack<BinarySearchTreeNode> stack = new Stack<>();
		while (binarySearchTreeNode != null) {

			// 右
			if (binarySearchTreeNode.right != null) {
				stack.push(binarySearchTreeNode.right);
			}

			// 左
			if (binarySearchTreeNode.left != null) {
				stack.push(binarySearchTreeNode);
				binarySearchTreeNode = binarySearchTreeNode.left;
			}
			else {
				// 中
				System.out.println(binarySearchTreeNode.value);
				if (stack.isEmpty()) {
					System.out.println("end");
					return;
				}
				else {
					binarySearchTreeNode = stack.pop();
					System.out.println(binarySearchTreeNode.value);		
				}
			
				if (binarySearchTreeNode.right != null) {
					binarySearchTreeNode = stack.pop();
				}
				else {
				}
			}
		}
	}
	
	public static void DFS_hindside(BinarySearchTreeNode binarySearchTreeNode) {

		Stack<BinarySearchTreeNode> stack = new Stack<>();
		while (binarySearchTreeNode != null) {

			// 中
			if (binarySearchTreeNode.left != null) {
				stack.push(binarySearchTreeNode);
			}
			
			// 右
			if (binarySearchTreeNode.right != null) {
				stack.push(binarySearchTreeNode.right);
			}

			// 左
			if (binarySearchTreeNode.left != null) {
				binarySearchTreeNode = binarySearchTreeNode.left;
			}
			else {

				System.out.println(binarySearchTreeNode.value);
				if (stack.isEmpty()) {
					System.out.println("end");
					return;
				}
				else {
					binarySearchTreeNode = stack.pop();
					System.out.println(binarySearchTreeNode.value);
					binarySearchTreeNode = stack.pop();
				}
			}
		}
	}

	public static void main(String[] args) {
		int[] treeNodeValues = new int[]{4,2,1,3,6,5,7};
		BinarySearchTreeNode root = new BinarySearchTreeNode(treeNodeValues[0]);
		for (int i = 1; i < treeNodeValues.length; i++) {
			root.add(treeNodeValues[i]);
		}

		DFS_forehead(root);
	}
}
