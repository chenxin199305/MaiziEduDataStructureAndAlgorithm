package lesson_006;

import lesson_002.Node;

/**
 *	笔记：
 *		约瑟夫环问题的本质其实是一个循环链表的问题 
 */
public class JosephCycle {

	public static Node josephCycleEliminate(Node startNode, int eliminateNum) {
		
		// 排除特例
		if (startNode == null || eliminateNum < 0) {
			return null;
		}
		
		Node currentNode = startNode;
		int count = 0;
		
		/**
		 *	笔记：
		 *		 当不只存在一个节点时，则进行算法过程
		 *		循环链表寻找 next 的过程不用担心 null 的问题，因为总是有 next 的
		 */
		while (currentNode.next != currentNode) {
			count++;
			if (count >= eliminateNum) {
				// 及时释放内存
				Node nextNode = currentNode.next;
				currentNode.next = currentNode.next.next;
				System.out.println("delete node = " + nextNode.value);
				nextNode = null;
				count = 0;
			}
			else {
				currentNode = currentNode.next;
			}
		}
		
		return currentNode;
	}
	
	public static void main(String[] args) {
		
		Node node1 = new Node(1);
		Node node2 = new Node(2);
		Node node3 = new Node(3);
		Node node4 = new Node(4);

		node1.next = node2;
		node2.next = node3;
		node3.next = node4;
		node4.next = node1;
		
		Node result = josephCycleEliminate(node1, 3);
		System.out.println(result.value);
	}
}
