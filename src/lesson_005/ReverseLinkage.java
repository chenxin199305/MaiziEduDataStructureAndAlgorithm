package lesson_005;

import lesson_002.Node;

public class ReverseLinkage {

	public static Node node;
	public static Node newHeadNode;

	/**
	 *	笔记：
	 *		做递归算法的时候，需要注意这么一个点：
	 *			1. 如果所要执行的算法是从头部开始，那么先写执行的功能，后调用递归的函数
	 *			2. 如果所要执行 的算法是从尾部开始，那么调用递归的函数，后写执行的功能。
	 */
	public static Node reverseLinkageUsingRecursion(Node head) {
		
		// 排除特例
		if (head == null) {
			return null;
		}
		
		if (head.next != null) {
			Node nextNode = reverseLinkageUsingRecursion(head.next);
			nextNode.next = head;
			return head;
		}
		else {
			newHeadNode = head;
			return head;
		}
	}
	
	/**
	 *	笔记：
	 *		对于链表不使用递归时，要时常保证头结点的指针的控制权
	 *		以及三个连续节点指针的控制权。 
	 */
	public static Node reverseLinkageUsingLoop(Node head) {
		
		// 排除特例
		if (head == null || head.next == null) {
			return head;
		}
		
		Node preNode = null;
		Node midNode = null;
		Node behNode = null;
		behNode = head;
		midNode = head.next;
		preNode = head.next.next;
		behNode.next = null;
		while (preNode != null) {
			midNode.next = behNode;
			behNode = midNode;
			midNode = preNode;
			preNode = preNode.next;
		}
		
		// 最尾部的执行
		midNode.next = behNode;
		
		return midNode;
	}
	
	public static void main(String[] args) {
		
		Node node1 = new Node(1);
		Node node2 = new Node(2);
		Node node3 = new Node(3);
		Node node4 = new Node(4);
		Node node5 = new Node(5);

		node1.next = node2;
		node2.next = node3;
		node3.next = node4;
		node4.next = node5;
				
//		reverseLinkageUsingRecursion(node1);
//		node1.next = null;
		newHeadNode = reverseLinkageUsingLoop(node1);
		
		Node tempNode = newHeadNode;
		while (tempNode != null) {
			System.out.print(tempNode.value + " ");
			tempNode = tempNode.next;
		}
	
	}
}
