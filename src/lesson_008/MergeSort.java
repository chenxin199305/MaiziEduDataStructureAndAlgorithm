package lesson_008;

public class MergeSort {

	/**
	 *	函数说明：	将已经排序好的两个数组进行合并 
	 *
	 *	@param		array1		第一个要链接的数组
	 *	@param		array2		第二个要链接的数组
	 */
	public static int[] linkArray(int[] array1, int[] array2) {
		
		int[] result = new int[array1.length + array2.length];
		int array1Pointer = 0;
		int array2Pointer = 0;
		int resultArrayPointer = 0;
		
		while(array1Pointer < array1.length || array2Pointer < array2.length) {
			
			// 如果数组1到头了
			if (array1Pointer == array1.length) {
				for (; array2Pointer < array2.length; array2Pointer++, resultArrayPointer++) {
					result[resultArrayPointer] = array2[array2Pointer];
				}
				break;
			}
			
			// 如果数组2到头了
			if (array2Pointer == array2.length) {
				for (; array1Pointer < array1.length; array1Pointer++, resultArrayPointer++) {
					result[resultArrayPointer] = array1[array1Pointer];
				}
				break;
			}
			
			if (array1[array1Pointer] < array2[array2Pointer]) {
				result[resultArrayPointer] = array1[array1Pointer];
				array1Pointer++;
				resultArrayPointer++;
			}
			else {
				result[resultArrayPointer] = array2[array2Pointer];
				array2Pointer++;
				resultArrayPointer++;
			}
		}
		
		return result;
	}
	
	/**
	 *	函数说明：	对数组进行递归二分，排序
	 *
	 *	笔记：
	 *			该方法使用了递归
	 */
	public static int[] seperateAndSortArrayUsingRecursive(int[] array) {
		
		int[] result = new int[array.length];
		
		if (array.length < 2) {
			return array;
		}
		else {
			int mid = array.length / 2;
			int[] left = new int[array.length / 2];
			int[] right = new int[array.length - mid];
			
			// 拷贝备份左右两部分
			for (int i = 0; i < left.length; i++) {
				left[i] = array[i];
			}
			for (int i = 0; i < right.length; i++) {
				right[i] = array[array.length - mid + i];
			}
			
			// 分别进行二分和排序
			left = seperateAndSortArrayUsingRecursive(left);
			right = seperateAndSortArrayUsingRecursive(right);
			result = linkArray(left, right);
		}
		
		return result;
	}
	
	public static int[] seperateAndSortArrayUsingLoop(int[] array) {
		
		int length = array.length;
		int[] result = new int[length];
		
		/**
		 *	算法：
		 *			非递归的二分 + 合并排序
		 *			通过每次调整 step 的大小，确定不同的排序输入数组 
		 */
		for (int step = 1; step < length; step = step * 2) {
			for (int left = 0; left < length; left = left + 2 * step) {
				int middle 	= Math.min(left + step, length - 1);
				int right 	= Math.min(left + 2*step, length - 1);
				
				// 创建临时数组
				int[] leftArray 	= new int[middle - left];
				int[] rightArray	= new int[right - middle + 1];
				for (int i = left; i < left + leftArray.length; i++) {
					leftArray[i - left] = array[i];
				}
				for (int i = middle; i < middle + rightArray.length; i++) {
					rightArray[i - middle] = array[i];
				}
				
				/**
				 *	疑问：
				 *			使用数组链接的前提要求是数组已经排好序？ 
				 */
				int[] tempArray = linkArray(leftArray, rightArray);
				for (int i = left; i < left + leftArray.length + rightArray.length; i++) {
					array[i] = tempArray[i - left];
				}
			}
		}
		
		return result;
	}
	
	/**
	 *	笔记：	归并排序算法 
	 */
	public static int[] mergeSort(int[] array) {
		
		int[] result = new int[array.length];
		
		result = seperateAndSortArrayUsingLoop(array);
		
		return result;
	}
	
	public static void main(String[] args) {
		
		int[] array = new int[]{2,3,4,5,6,3,2,2,3,4,5,4,3,2,1,9};
		
		int[] sortArray = mergeSort(array);
		
		for (int i = 0; i < sortArray.length; i++) {
			System.out.print(sortArray[i] + " ");
		}
	}
}
