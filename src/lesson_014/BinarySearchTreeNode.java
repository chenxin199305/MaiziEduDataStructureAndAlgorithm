package lesson_014;

public class BinarySearchTreeNode {
	
	public BinarySearchTreeNode left;
	public BinarySearchTreeNode right;
	public int value;
	
	public BinarySearchTreeNode(int value) {
		this.value = value;
	}
	
	/**
	 *	函数说明：	给搜索二叉树提供新数据 
	 */
	public void add(int value) {
		
		if (this.value == value) {
			return;
		}
		
		if (this.value > value) {
			if (this.left != null) {
				this.left.add(value);
			}
			else {
				BinarySearchTreeNode binarySearchTreeNode = new BinarySearchTreeNode(value);
				this.left = binarySearchTreeNode;
			}
		}
		else {
			if (this.right != null) {
				this.right.add(value);
			}
			else {
				BinarySearchTreeNode binarySearchTreeNode = new BinarySearchTreeNode(value);
				this.right = binarySearchTreeNode;
			}
		}
	}
	
	/**
	 *	函数说明：	搜索特定的值 
	 */
	public void search(int target) {
		
		if (this.value == target) {
			System.out.println("this node has the target value of " + target);
			return;
		}
		
		if (this.value > target) {
			if (this.left != null) {
				System.out.println("left");
				this.left.search(target);
			}
			else {
				System.out.println("this tree has no such value: " + target);
				return;
			}
		}
		else {
			if (this.right != null) {
				System.out.println("right");
				this.right.search(target);
			}
			else {
				System.out.println("this tree has no such value: " + target);
				return;
			}
		}
	}
	
}
