package lesson_020;

import java.util.Stack;

public class BiaoDaShiQiuZhi {

	/**
	 *	函数说明：	对输入的字符串进行解析 
	 */
	public static BinaryByteTree parse(String biaodashi) {

		BinaryByteTree binaryByteTree = null;
		byte[] biaodashibytes = biaodashi.getBytes();
		Stack<BinaryByteTree> stack = new Stack<>();
		for (int i = 0; i < biaodashibytes.length; i++) {
			if (biaodashibytes[i] >= '0' && biaodashibytes[i] <= '9') {
				stack.push(new BinaryByteTree(biaodashibytes[i]));
			}
			else {
				binaryByteTree = new BinaryByteTree(biaodashibytes[i]);
				binaryByteTree.right = stack.pop();
				binaryByteTree.left = stack.pop();
				stack.push(binaryByteTree);
			}
		}
		binaryByteTree = stack.pop();
		return binaryByteTree;
	}
	
	/**
	 *	函数说明：	对构成的计算二叉树进行计算 
	 */
	public static int calculate(BinaryByteTree binaryByteTree) {
		
		int result = 0;
		
		result = median(binaryByteTree);
		
		return result;
	}
	
	/**
	 *	函数说明：	二叉树中序遍历 
	 */
	public static int median(BinaryByteTree binaryByteTree) {
		if (binaryByteTree.left == null && binaryByteTree.right == null) {
			return (int)(binaryByteTree.value - '0');
		}
		
		int left = median(binaryByteTree.left);
		int right = median(binaryByteTree.right);
		
		if (binaryByteTree.value == '+') {
			return left + right;
		}
		else if (binaryByteTree.value == '-') {
			return left - right;
		}
		else if (binaryByteTree.value == '*') {
			return left * right;
		}
		else if (binaryByteTree.value == '/') {
			return left / right;
		}
		else {
			return 0;
		}
	}

	public static void main(String[] args) {
		String biaodashi = "45+9*";

		System.out.println(median(parse(biaodashi)));
	}
}
