package lesson_020;

import lesson_014.BinarySearchTreeNode;

public class BinaryByteTree {
	
	public BinaryByteTree left;
	public BinaryByteTree right;
	public byte value;
	
	public BinaryByteTree(byte value) {
		this.value = value;
	}
	
}
